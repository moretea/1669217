# routes.rb is the same

# app/controllers/application_controller.rb
class ApplicationController < ActionController::Base
  include ApplicationHelper # Force it to be present in all controllers
  before_filter :find_login_and_account
end

# app/helpers/application_helper.rb
module ApplicationHelper
  def find_login_and_account
    # Try to find the login.
    Login.current = @login = Login.find_by_id(session[:login_id])

    Account.current = @account = \
      if Login.current.present?
        # Try to find the account (accessible from the current login), if the user is logged in
        Login.current.accounts.find_by_id(params[:account_id])
      else 
        # User is not logged in, so account must be nil.
        nil
      end
  end

  def logged_in?
    @login.present?
  end

  # Simple helper used in e.g. layout to show menu options. It should return true in the ThingsController
  def account_selected?
    (not @account.nil?) && (not @account.new_record?)
  end
end

# app/controllers/things_controller.rb
class ThingsController < ApplicationController
  def index
    @account.things
    # could also use
    Account.current.things
  end
end